package com.example.marcio.gasolinaoualcool;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText precoAlcool;
    private EditText precoGasolina;
    private Button btnVerificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        precoAlcool = findViewById(R.id.precoAlcoolId);
        precoGasolina = findViewById(R.id.precoGasId);
        btnVerificar = findViewById(R.id.angry_btn);

        btnVerificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txtPrecoAlcool = precoAlcool.getText().toString();
                String txtPrecoGasolina = precoGasolina.getText().toString();

                Context contexto = getApplicationContext();
                int duracao = Toast.LENGTH_LONG;

                if(!txtPrecoAlcool.isEmpty() && !txtPrecoGasolina.isEmpty()){
                    Double alcool = Double.parseDouble(txtPrecoAlcool);
                    Double gasolina = Double.parseDouble(txtPrecoGasolina);
                    Double resultado = alcool/gasolina;
                    if(resultado >= 0.7){
                        String texto = "Abastecer com GASOLINA é mais econômico.";
                        Toast toast = Toast.makeText(contexto, texto, duracao);
                        toast.show();
                    } else {
                        String texto = "Abastecer com ÁLCOOL é mais econômico.";
                        Toast toast = Toast.makeText(contexto, texto, duracao);
                        toast.show();
                    }
                } else {
                    String texto = "Preencha os campos com os valores dos preços dos combustíveis.";
                    Toast toast = Toast.makeText(contexto, texto, duracao);
                    toast.show();
                }
            }
        });
    }
}
